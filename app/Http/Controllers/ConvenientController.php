<?php

namespace App\Http\Controllers;

use App\Models\Convenient;
use Illuminate\Http\Request;

class ConvenientController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Convenient $convenient)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Convenient $convenient)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Convenient $convenient)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Convenient $convenient)
    {
        //
    }
}
